The Original Round Table Pizza Richmond is an award winning pizza restaurant that has been serving the Richmond community for almost 30 years. We always use the freshest ingredients and pizza delivery is always free to anywhere in Richmond. Located at Garden City and Blundell. Call Now: 604-275-4325!

Address: 160-8780 Blundell Road, Richmond, BC V6Y 1K1, Canada

Phone: 604-275-4325

Website: http://roundtablepizzarichmond.ca
